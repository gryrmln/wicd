#!/usr/bin/env bash
export LANG='C'
export NO_AT_BRIDGE=1
find . ! -path '.git' -type f -iname '*.py' -exec pylint -E \{\} \;
